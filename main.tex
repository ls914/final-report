\documentclass[10pt]{article}
\usepackage[titletoc]{appendix}
\usepackage{import}
\import{./utils/}{utils.TeX}
% % if table is used uncomment this below
% % \captionsetup[table]{position=bottom}   

% \darkmode

\newcommand{\lrtitle}{IDP M102 Final Report}
\date{\today}


\begin{document}

\createtitlepage
\TOC{gray}


\section{Overview}
Over the years, robots have become more proficient at completing jobs that involve physically demanding manual labour. As a team, we have come up with a plan for designing and constructing a robot that will be able to identify and sort blocks, distinguishing between one containing mild steel and the other not. This may be used in industry for jobs involving waste sorting, recycling, or removing hazardous components.  Through design briefings and collaboration as teammates, we produced a Gantt chart to organise and set up definitive deadlines and milestones. By assessing these, we were able to brainstorm through key concepts and possible solutions. All of which will hopefully result in a working robot that is able to complete the tasks set out for us.

\section{Introduction}
“A technology demonstrator robot should be built to show the potential of robots for sorting pre-packaged waste for further processing. Clean waste has no metallic content and should if delivered to a different area from waste which contains ferrous metal (steel) and requires additional processing.”
Source: \href{https://www.vle.cam.ac.uk/pluginfile.php/18410181/mod_resource/content/4/2021_M1\%20Task\%20V1-1.pdf}{‘M1 Task: Waste Sorting’ Handout}

\section{System Summary}
\subsection{Overall Desgin}

\autoref{fig:sys-diagram} (in \autoref{appendix:overall}) above shows the general systems level diagram, centred around the Arduino Uno Wifi rev2 and the Adafruit Motorshield v2. All of our subsystems are shown with their connections to the Arduino, ranging from those that include intricate circuitry to just the output pins of the module itself. There is also a general specification list, which shows the formal requirements and constraints set by the task. These can be found in \autoref{appendix:overall}.

\img{./img/concept.png}{.8}{Concepts considered for each aspect of the robot’s core tasks}{concept}

\noi \textbf{Wheel configuration}: It was decided to use two wheels and a ball bearing, as this configuration gave more freedom in movement, and the limitation of catching on the ramp could be overcome by moving the ball bearing. 
Navigate across the arena: Although the solution of navigating along the walls would have fewer failure conditions, and so require less designing for redundancy, we decided to utilise line-following across the ramp. This is because the line following method will allow for easier location of the collection area and box deposit areas, taking less time to travel. We attempted to utilise a proportional control circuit using two line sensors to achieve this, which is explained in 3.3.
\\[1em]
\textbf{Detect block in front of the robot}: A push switch was unsuitable, as the range of detection would be too small, and could lead to situations where the robot drives into the block but does not sense it. Although the ultrasound sensor has a wider range of vision, it cannot give an indication of where in that range the detected object lies. Therefore, infrared was decided to be a better fit with the narrower field of vision. 
\\[1em]
\textbf{Detect whether a block has metal or not}: Weight difference was quickly eliminated as a detecting mechanism, as the difference in weight between the blocks was below our tolerances. The inductive proximity detector was decided on, as it would give the greatest sensitivity for detecting the blocks; in comparison to using an electromagnetic sensor, which produces a weaker signal. 
\\[1em]
\textbf{Pick up and transport the block}: We wanted to make this mechanism as simple as possible, by minimising the use of spare motors or gear systems. The result of which was a block dragging module that is attached to a single servo motor, which can be activated by simply setting the angle of the servo motor in the software.



\subsection{Mechanical}

\subsubsection{Chassis}

\twoimg{./img/mech/Robot.png}{.81}{Robot}{robot}{./img/mech/Ball-Bearing.png}{.475}{Ball Bearing}{ball-bearing}

\noi
The chassis was designed out of 4mm MDF wood to be as light as possible. We chose MDF wood because our designs were simple enough to laser cut, which is quicker than other forms of fabrication, meaning we could develop and change our chassis rapidly.
\\[1em] 
The shape of the floor was chosen to allow for enough space for the components, whilst also being efficient with material usage and weight, hence the back was shorter in length than the front, since we shaved off the excess floor, as shown in \autoref{fig:robot} We chose not to have side walls, since they would have been unnecessary and would have added to the weight, but we opted to have a front wall, such that the ultrasonic sensor could be housed easily with its height fixed in its desired place, as shown in \autoref{fig:robot}.
\\[1em]
The motors were stationed at the front to drive the robot, and a ball bearing at the back to support it and make sure the floor was parallel to the ground, such that the line sensors were easy to position as desired. In order to make sure that the floor was parallel to the ground, we also implemented another 4mm piece of MDF wood, between the floor and ball bearing to lower the ball bearing slightly, as shown in \autoref{fig:ball-bearing}. All components were located in line or on top of our pivots (the motors and ball bearings), to ensure that moments were balanced to prevent tipping and that the floor was not subject to excess strain.



\subsubsection{Positioning}
\label{sec:postioning}


\twoimg{./img/mech/Chassis-Floor.png}{.5}{CAD model of chassis floor}{chassis-floor}{./img/mech/Floor.png}{.5}{Chassis floor}{floor}
\FloatBarrier
We originally had the line sensors placed centrally with respect to the chassis floor. In testing, we found that this meant that the sensors swung out of phase with the robot – as the robot turned the line sensors would still be stationed on top of the line, causing our robot to continue to turn past its desired point. This is why we opted to position the front line sensors slightly in front of the wheels, to make sure that they moved in phase with the robot, as shown in our Solidworks floor schematic in \autoref{fig:chassis-floor}.

\img{./img/mech/USIR.png}{.3}{Ultrasound and IR sensor}{USIR}

\noi
The infrared sensor was bracketed low to the ground as shown in \autoref{fig:USIR}, since it had to detect the blocks. It was positioned slightly in front of the front wall of the chassis, to allow space for the line sensors to be in front of the wheels and to stop it from catching on the ramp, which we found to be a problem with our previous testing rig, in which the lines sensors were in line with the front of our chassis.
\\[1em]
The metal detector and block dragging mechanism were positioned at the back of our robot. In testing we found that the blocks could be pulled/dragged onto the ramp successfully, but trying to push the block onto the ramp would result in it getting caught. Therefore, our strategy was to detect the block with the front IR sensors, then turn our robot 180 degrees, such that the block was lined up to be collected by our mechanism

\subsubsection{Brackets}

\twoimg{./img/mech/Line-Sensor-Bracket.png}{.565}{Linesensor Bracket}{ls-bracket}{./img/mech/Motor-Bracket.png}{.47}{Motor Bracket}{m-bracket}	\FloatBarrier

\img{./img/mech/Coil-Screwed-into-Bracket.png}{.6}{Coil Screwed into Bracket}{coil-bracket}

\noi
Our previous motor brackets, used in testing, showed us that the metal bracket must be accommodating enough for the motor’s shaft to prevent friction and rubbing, since this would cause wheel slippage and result in unepected motion. As a result, our final motor brackets had a hole large enough for free rotation of the shaft, whilst also being very stable since the motor was able to be screwed into place, either side of the shaft. It is presented in \autoref{fig:m-bracket}. We chose to make it out of sheet metal and drill/punch holes into. The servomotor which drove our block pickup mechanism was also bracketed onto the back with a sheet metal piece.
\\[1em]
With the line sensor brackets we were previously , we found that whilst it was very stable, the position of the line sensors were quite invariable – we could not move our sensors closer/farther to the ground between testing runs. This was detrimental, as the line sensing was very sensitive to and dependent on the sensors’ height above the ground. Therefore, instead, we opted for a new metal line sensors bracket, (\autoref{fig:ls-bracket}) in which the line sensors could be screwed into place. When screwed tight, the sensors were held in place as desired, and when the screw was loosened, we were able to freely adjust the line sensors’ positions.          

\subsubsection{Block Dragging Mechanism}

\img{./img/mech/Coil,Mechanism.png}{.5}{Coil Mechanism}{coil-mech}

\noi
In \autoref{fig:coil-mech} you can see our coil wrapped around an MDF wood cylinder held in place with 2 zip ties. We opted to wrap the coil around wood to make sure it did not collapse and that enough room was left for the block to be placed into the coil. The mechanism is attached to the servomotor as shown in (\autoref{fig:coil-bracket}) with 3mm MDF wood acting as a lever arm to rotate the coil into place around the block and drag it along. As stated previously, we opted to drag the block, since this ensured it did not get caught on the ramp. By having the coil as part of this mechanism, we did not have to implement another system to get the block into place within the coil.


\subsection{Electrical}

\subsubsection{Line Following}
The main considerations in the system for line detection were the number, and placement of sensors, and the way in which they were used.
\\[1em]
The two main ways of using the sensors were building a digital circuit to give a binary output from each sensor, or utilizing the analogue signal from each sensor to compare the values from multiple sensors. This method would give a form of proportional control in our line following program. The digital output circuits would be a simpler form of control, as the circuit would use a number of comparator Op-Amps, and would give Booleans to work with. However, the digital circuit would give much less sensitivity in the control, having to use more sensors to create a form of proportional control. The benefits of using the analogue sensor values are that it would allow for more precise control of the robot, as smaller deviations could be corrected with smaller motor movements. The analogue circuit, on the other hand, is much more sensitive to sensor height. This means that the sensor placement has to be much more precise than when using a digital circuit. Also, as the analogue circuit only gives an output of the difference between the sensors, they cannot be used to detect the presence of a line.
\\[1em]
The final decision was to use the analogue circuit with two light dependent sensors. This was because it gave more precise control of the robot with using fewer sensors, allowing the remaining sensors to be used for other purposes.
\\[1em]
There were two other light dependent sensors used in the design. One was used as a front line sensor to detect when the robot has left the line in order to implement the algorithm to return to the line. The other sensor was used at an offset distance from the front line sensor to detect when the robot has reached a junction.
\\[1em]
These circuits (\autoref{fig:front-line}, \autoref{fig:junction}, \autoref{fig:prop-control}) are shown in the \autoref{appendix:electrical}.

\subsubsection{Block detection}

The considerations when finding out how to detect the block were which sensor to use, and at what position to place the sensor. As all of the light dependent sensors were assigned to other sensors, it was not possible to use one with a filter to detect the presence of a block. The ultrasound sensor has the benefit of a wider field of vision, which would allow for quicker sweeping of the collection area in order to detect the block. However, because of the wider field of vision, it is more difficult to ascertain the position of the block in the collection area. The narrower range of vision of the infra-red sensor allows for easier detection of the position of the block, and can still carry out relatively quick sweeps of the collection area.
\\[1em]
Although having the sensor facing downwards hanging over the front of the chassis would enable the detection of only blocks, it necessitates the use of a different sensor or methodology to detect the block in the collection area in the first place, or otherwise conduct a much more laborious sweep of the collection area. To account for the fact that the front sensor will also pick up obstacles, it was important to write an algorithm for the robot to move from side to side when it detects an obstacle. If the obstacle is still detected on either side, it is safe to assume that it is an obstacle rather than a block.
\\[1em]
Because of these factors, the decided method was to use an infra-red sensor at the front of the chassis to detect the block.

\subsubsection{Metal detection}

The possible methods of detecting the presence of metal in the block include using the deflection of a magnet in the presence of a metal containing block, using a magnetic field sensor to detect the resulting field when the metal in the block is magnetised by a coil, or detecting the changes in inductance of a coil resulting from eddy currents in the metal. Measuring the deflection of a magnet was unfeasible, as the resulting deflection would be less than that caused by the movement of the robot itself. The detection of magnetic fields was also not sensitive enough for the metal in the blocks. 
\\[1em]
For the eddy current sensing, a single coil was used rather than a generator and detector coil. This was because the detection was strongly sensitive to the placement of the detector coil in relation to the generator coil.
\\[1em]
The final circuit (\autoref{fig:metal}) is shown in the \autoref{appendix:electrical}.

\subsection{Software}

\subsubsection{Tools}
The software controls every aspect of the robot to produce an output that meets the goals and criteria of the task. This links together all the individual aspects of the robot (Mechanical and Electrical) and makes them work together, so it is trivial that we should use the best tools to create amazing software.
\\[1em]
As a result, we are using the \href{https://platformio.org/}{PlatformIO} environment (instead of the Arduino IDE) since it has superior IntelliSense and linting allowing us to write code faster and spot bugs quicker. Git and GitHub is used for version control and collaboration. 

\subsubsection{Folder Strucutre}

The folder structure (\autoref{fig:folder-struc} in \autoref{appendix:software}) that we adopted was chosen as it allowed us to abstract out code concerning different areas of the robot (IMU, Wi-Fi, etc.) into their own files. This will reduce the clutter in the main.cpp file, and it makes life easier when you want to debug a certain part of the robot. 

\subsubsection{Classes}

\img{./img/soft/UML.png}{.8}{UML Diagram of Classes}{classes}
\noi
As mentioned in the section above we have split different areas of the robot into their own folders, the different areas are implemented as C++ classes. Classes allow our code to be more abstract and readable, and as it groups code together, we can easily find and fix bugs. 
\\[1em]
\autoref{fig:classes} shows clearly how all the classes interact with each other. 

\subsubsection{Navigation and Movement}
We have three systems that plan to include in the robot movement algorithm. The line-following circuit is the main circuit that we will be using to control motion, but we also have an ultrasound sensor preventing the robot from crashing into a wall. In addition, IMU will retrieve the acceleration and use numerical integration to calculate pitch, yaw and roll of the robot (which is used for ramp detection). Combining all these together will allow us to redundantly track the position of the robot.

\subsubsection{Decision-making}

The general block collection algorithm is illustrated in the flow chart (\autoref{fig:soft-flow}) found in \autoref{appendix:software}.
\\[1em] 
There are many instances of the robot having to ignore code depending on the circumstances. For example, when the robot reaches the white border of the starting box, or when the robot reaches the intersection between the red and blue box when it does not yet have the block picked up, or when the robot detects a block when in fact it is in front of an obstacle. We want to implement a class, \texttt{RobotDecisions} that can make these decisions for itself. From \autoref{fig:classes} can see how most of the components of the robot are passed in as parameters into the methods of \texttt{RobotDecisions}, this allows us to track/monitor what each algorithm depends on, so in case, a specific algorithm stops working, we can track down exactly which dependency stopped working. This came in very useful as we were able to quickly track down when our ultrasound stopped working during testing.
\\[1em]
To overcome the issue of the line-following unit misbehaving when the robot crosses a junction, we will include a series of if-statements to, continue moving forwards when it is in the starting half of the arena, but no block has been picked up. Once the block has been picked up and the robot crosses the junction in the starting half of the arena, we can run an alternative algorithm which stops and sorts the block into its respective boxes.
\\[1em]
If the robot finds itself in front of an obstacle (such as the yellow walls), the robot will believe it has found a block and may attempt to pick it up. To fix this, we can define a function that rotates the robot in an arc either side of the object and if the sensor relays that the ‘block’ can still be detected, it can conclude that it has in fact, come across an obstacle and skip the program that picks it up.
\\[1em]
There is a small likely hood that the robot may go off the line, so to fix this issue we have recovery stage algorithms. If the front-line sensor does not detect a line, then the robot will go through stages of sweeping from left to right until it re-finds the line, then it exits the stages, and returns to its original line following algorithm. 

\subsubsection{WiFi Logging}

One of the first ideas we came up with was to have data logging through Wi-Fi. The main benefit from this is that we will be able to remotely analyse the robots' movements. The robot may think it's doing something, but in practice it won’t do it, this a very common issue that will occur, and having a live view of what the robot is thinking of doing, will aid us significantly in finding bugs in our code
\\[1em]
The WiFi section on \autoref{fig:sys-diagram} (in \autoref{appendix:overall}) demonstrates the unidirectional communication between the Arduino and a computer. The way this system works is that the Arduino sends a HTTP request (in the form of a GET or POST request), to the server running on the computer. The server will listen for requests, and when a request is received, it will figure out where to store that data to onto the database using the URL endpoint that the request was made to. The frontend (a website) will fetch the data every 100ms and display the data on graphs or textboxes. 
\\[1em]
There are some drawbacks to using Wi-Fi. 

\bul{
	\item Wi-Fi libraries increase the bundle size of our code by 10.5KB which is 22\% of the Arduino’s max capacity
	\item Initial 8s waiting time for the robot to establish a connection to Wi-Fi
	\item ~300ms delay between the Arduino sending the data, and the data being updated on the frontend
	\item HTTP requests introduce a 100ms delay in the Arduino loop
	\item Evaluating status codes adds a 2s delay
}

\noi The major drawback is the last one, as a 2s delay will significantly reduce the speed at which our robot can make decisions, thus we turn off status code evaluation, which does mean that the robot will continue to send data even if the server on the computer is down, but this does not affect the performance of the robot.
Also, many fail safes were added to ensure that the robot will continue to move, in case it can’t connect to WiFi or it has trouble sending HTTP requests, thus making it very redundant. 



\section{Robot testing}
\subsection{Subsytem testing}
The implementation of the WiFi from the Arduino to the computer proved to be very useful in the testing and debugging of the subsystems. It allowed us to run the robot remotely whilst having the benefit of seeing the serial printed outputs from each module, allowing us to see what the robot is seeing and at what point it does. It was especially helpful when we tried to recalibrate the proportional control circuit, as the line sensor were incredibly sensitive to its vertical placement.

\subsubsection{Ultrasound sensor}
The ultrasound sensor was one of the first sensors to be worked on. It was initially used as a general sensor to detect the presence of a block (after which it would be pinpointed by the IR sensor), as well as a precautionary measure against colliding against a wall or obstacle. During the initial stages of testing, we were able to retrieve the signals and calibrate them successfully to read the distance of a wall to an appropriate level of accuracy in centimetres. Despite no changes made to the program, later in the project we found that this was no longer functional even with new sensors.

\subsubsection{Infrared sensor}
We had two IR sensors the A21 and A02, originally both worked excellently, and they were calibrated quite well. However, we managed to destroy our A21, which wasn’t an issue as we weren’t using it. The A02 continued to work perfectly for the entirety of the project, and the only changes we had to do were to re-calibrate it near the competition times, to ensure its accuracy was the best.
\subsubsection{Front-line and junction detection sensors}

The front-line and junction detection sensors were essentially the same type of circuit; they would output a digital high value when they were detecting a line. We had almost no problems when it came to testing this throughout the duration of the project, with only a quick realisation during the first test that these sensors had to be very close to the ground. However, by the first competition the line sensors themselves seemed to no longer function and we had spent long time figuring out the analogue outputs from the sensors – it was concluded that somehow the sensors themselves were broken and we were denied new sensors.

\subsubsection{Proportional control}

We had a lot of issues with proportional control. 
\\[1em]
During initial development of proportional control on the breadboard, it was tested extensively to figure out exactly at which height it works. However, when it came to mounting the line sensors on the final chassis, we found out the height at which it worked varied a lot, and it was highly susceptible to small movements. This mean that every day we will come in, find out the proportional control doesn’t work, then we have to go through the 40min process of finding the exact position at which the sensors worked. 
\\[1em]
We threw 2 weeks of coming in early, and staying well past out timetable session, to try an fix this. However, we could not get it to work consistently. At which point, we decided to use the proportional control sensors as if it was digital, by setting a threshold of the digital LEFT or RIGHT value. This worked, and with the combination of the front-line sensor we successfully managed to get line following with our set up. Unfortunately, somehow through the process of moving the sensors to the final circuit board, we managed to break the sensors as they failed to produce any voltages when testing them with the picoscope. This rendered us with no line following. 


\subsubsection{Metal detector}
The metal sensing circuit was tested thoroughly when on the breadboard, and it worked as expected. In the software we had logging of the important variables, to debug and check to see if they are correct. However unfortunately, the metal sensing circuit on the perfboard failed to work, and even after checking connections with the multimeter, we could not narrow down what was wrong with it.
\subsubsection{Toggle switch}
The toggle switch was surprisingly difficult to get working. A major issue we had was bouncing, and through trial and error we managed to tune the debounce time in the software to get it adequately working. The toggle switch was extensively tested with the system connected to 12V through a power adapter and it worked perfectly, however at the very last minute we switched to the battery, and it seemed to fail. We could not resolve this issue in time for the competition, hence we had to bypass this switch for the final competition.
\subsubsection{Block dragging servo mechanism}
From the software side, a lot of testing was done to determine the angles at which the servo works at and delay required for smooth operation. Using this, we were able to develop a sweep function that successfully drops the block collection. Additionally, the method \texttt{SweepTest()} was available for use, which let us  to quickly test the servos range.
\subsubsection{LEDs}
The LEDs were very simple to test, as it only required only required digital writing to get them to light up. In case that didn’t work, we resorted to testing then using the picoscope to make sure a high signal is being applied at the correct time.

\subsection{Complete robot testing}
We had great difficulty in testing the robot with all the subsystems interacting with each other - the primary reason being the failure of our line-following. Testing of the many scenarios such as junction detection and block detection relied on the robot to stay on the line and not steer away.

\section{Major Project Decisions}
When we encountered the problem with our proportional control circuit, we ended up focusing everything on this for two weeks and managed to make this work only to see it fail the day after multiple times. We were fooled by a sense of hope, but this lasted too long and as a team we should have agreed to stop and start from the beginning with a new and less complex design.
\\[1em]
An earlier iteration of the robot chassis included thick 6mm MDF walls with holes just large enough to fit the axle of the motor through. The intention was to use the wall to fix the lateral positioning of the motor arm, but the thickness meant that the screw on the motor axle was scraping against this and reducing the drive. We removed the side walls to both eliminate this problem and to reduce the overall weight. Another change made to the robot chassis was the positioning of the line sensors to the base, which was discussed previously in \ref{sec:postioning}.

\section{Team Management}
We adopted a collaborative team management system, with two project managers, Jiajiong Liu and Frank McMullan. This allowed us to have multiple viewpoints on the best design and management strategies while still being an efficient system. The main strategy was for the project managers to set deadlines for each subsystem group (Mechanical, Electrical, Software), in order to have a structured design process. This made sure that the teams did not work on unnecessary areas, and that the different design systems could be brought together easily.
\\[1em]
Unfortunately, this approach was not widely successful. Part of the reason for this was the loss of a crucial team member on the electrical team early on in the competition. This gave our team reduced manpower, particularly in the electrical subsystem, meaning that the hoped-for deadlines were not able to line up with the rest of the subsystems. The reduced workforce resulted in further delays in the project, as the other subsystems were unable to progress in many areas without the required electrical systems. An attempt was made to rectify this by merging the electrical and software teams, particularly to work on the line-following system. This change, however, came too late in the project to make a great enough impact to get the project back on schedule.

\section{Failure Analysis}

The robot did not meet the specification of being started in a controlled manner, as we had a last minute failure of our toggle switch circuit; the toggle switch would only start the robot when connected to a plug point, and not the battery.
\\[1em]
The biggest failure in our competition was our inability to use the line-following circuit. The root cause of this failure was the loss of the electrical team member. Because the innovative proportional control circuit used was designed by that team member, who was then rendered unavailable, no other team member understood the exact workings of the circuit. Because of this, the circuit was very difficult to debug when testing and soldering, leading to large delays. One way that this could have been avoided would have been to abandon the proportional control circuit in favour of a simpler digital line following circuit, which would have been simpler to debug and implement.
\\[1em]
In addition, we encountered further failures in the ultrasound sensor and the metal sensor unit despite having written off those subsystems as functional since their respective individual subsystem tests. At the point of recognising the failure, we were already all focused on attempting to fix the proportional control circuit and did not have the time to extensively debug the issue, although all the connections and algorithms did not change. 


\newpage

\begin{appendices}

	\section{Overall}
	\label{appendix:overall}
	\img{./img/System-Diagram.png}{.9}{Overall System Diagram}{sys-diagram}
	\newpage
	\section{Electrical}
	\label{appendix:electrical}

	\img{./img/elec/Front-Line-Sensor.png}{.6}{Front Line Sensor}{front-line}
	\img{./img/elec/Junction-Sensor.png}{.6}{Junction Sensor}{junction}
	\img{./img/elec/Metal-Sensor.png}{.25}{Metal Sensor}{metal}
	\newpage
	\img{./img/elec/PropControl.png}{.8}{Proportional Control}{prop-control}
	\newpage
	\section{Software}
	\label{appendix:software}
	
	\img{./img/soft/SoftwareFlowchart.png}{.8}{Block Collection Flowchart}{soft-flow}
	\newpage
	\img{./img/soft/folder-struc.png}{.8}{Folder Strucutre}{folder-struc}


\end{appendices}
	


\end{document}
